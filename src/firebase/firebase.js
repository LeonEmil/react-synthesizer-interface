import firebase from 'firebase'

var firebaseConfig = {
    apiKey: "AIzaSyDnpRNB3qwQePuMZ8jMnJJVLEfGWBeUM7E",
    authDomain: "handball-manager-e6010.firebaseapp.com",
    databaseURL: "https://handball-manager-e6010.firebaseio.com",
    projectId: "handball-manager-e6010",
    storageBucket: "handball-manager-e6010.appspot.com",
    messagingSenderId: "487052079442",
    appId: "1:487052079442:web:156ff61bf042865d514a8b"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore()
export const auth = firebase.auth()
export const storage = firebase.storage()

export default firebaseConfig