let section = window.innerWidth / 9

const defaultPlayersBlue = [
    [
        {
            number: 1,
            position: {
                x: section * 4,
                y: 20
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 3,
                y: 20
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 5,
                y: 20
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 1,
                y: 20
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 2,
                y: 20
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 6,
                y: 20
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 7,
                y: 20
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 8,
                y: 20
            }
        },
    ],
]

export default defaultPlayersBlue