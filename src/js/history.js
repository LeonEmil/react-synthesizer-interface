let section = window.innerWidth / 9

const initialHistory = [
    {
        lines: [],
        defaultPlayersBlue: [[], [], [], [], [], [], [], []],
        defaultPlayersRed: [[], [], [], [], [], [], [], []],
        defaultObjects: [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []],
        customPlayers: []
    },
    {
        lines: [],
        defaultPlayersBlue: [
            [
                {
                    number: 1,
                    position: {
                        x: section * 4,
                        y: 20
                    }
                },
                {
                    number: 2,
                    position: {
                        x: section * 4,
                        y: 20
                    }
                },
                {
                    number: 3,
                    position: {
                        x: section * 4,
                        y: 20
                    }
                },
                {
                    number: 4,
                    position: {
                        x: section * 4,
                        y: 20
                    }
                },
                {
                    number: 5,
                    position: {
                        x: section * 4,
                        y: 20
                    }
                },
                {
                    number: 6,
                    position: {
                        x: section * 4,
                        y: 20
                    }
                },
                {
                    number: 7,
                    position: {
                        x: section * 4,
                        y: 20
                    }
                },
            ],
            [],
            [],
            [],
            [],
            [],
            [],
            [],
        ],
        defaultPlayersRed: [
            [
                {
                    number: 1,
                    position: {
                        x: section * 4,
                        y: window.innerHeight - 70
                    }
                },
                {
                    number: 2,
                    position: {
                        x: section * 4,
                        y: window.innerHeight - 70
                    }
                },
                {
                    number: 3,
                    position: {
                        x: section * 4,
                        y: window.innerHeight - 70
                    }
                },
                {
                    number: 4,
                    position: {
                        x: section * 4,
                        y: window.innerHeight - 70
                    }
                },
                {
                    number: 5,
                    position: {
                        x: section * 4,
                        y: window.innerHeight - 70
                    }
                },
                {
                    number: 6,
                    position: {
                        x: section * 4,
                        y: window.innerHeight - 70
                    }
                },
                {
                    number: 7,
                    position: {
                        x: section * 4,
                        y: window.innerHeight - 70
                    }
                },
            ],
            [],
            [],
            [],
            [],
            [],
            [],
            [],
        ],
        customPlayers: [
            {
                teamName: "",
                players: [
                    {
                        name: "",
                        number: ""
                    }
                ]
            },
            {
                teamName: "",
                players: [
                    {
                        name: "",
                        number: ""
                    }
                ]
            }
        ],
        defaultObjects: [
            [
                {
                    position: {
                        x: section * 7,
                        y: 60
                    }
                },
                {
                    position: {
                        x: section * 7,
                        y: 60
                    }
                },
            ],
            [
                {
                    position: {
                        x: section * 8,
                        y: 60
                    }
                },
                {
                    position: {
                        x: section * 8,
                        y: 60
                    }
                },
            ],
            [
                {
                    position: {
                        x: section * 7,
                        y: 90
                    }
                },
                {
                    position: {
                        x: section * 7,
                        y: 90
                    }
                },
            ],
            [
                {
                    position: {
                        x: section * 8,
                        y: 90
                    }
                },
                {
                    position: {
                        x: section * 8,
                        y: 90
                    }
                },
            ],
            [
                {
                    position: {
                        x: section * 7,
                        y: 120
                    }
                },
                {
                    position: {
                        x: section * 7,
                        y: 120
                    }
                },
            ],
            [
                {
                    position: {
                        x: section * 8,
                        y: 120
                    }
                },
                {
                    position: {
                        x: section * 8,
                        y: 120
                    }
                },
            ],
        ]
    }
]

export default initialHistory