let section = window.innerWidth / 9

const defaultPlayersRed = [
    [
        {
            number: 1,
            position: {
                x: section * 4,
                y: window.innerHeight - 70
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 3,
                y: window.innerHeight - 70
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 5,
                y: window.innerHeight - 70
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 1,
                y: window.innerHeight - 70
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 2,
                y: window.innerHeight - 70
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 6,
                y: window.innerHeight - 70
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 7,
                y: window.innerHeight - 70
            }
        },
    ],
    [
        {
            number: 1,
            position: {
                x: section * 8,
                y: window.innerHeight - 70
            }
        },
    ],
]

export default defaultPlayersRed