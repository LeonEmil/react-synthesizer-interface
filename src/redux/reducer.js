import * as actions from './actionTypes'
import defaultPlayersBlue from './../js/defaultPlayersBlue'
import defaultPlayersRed from './../js/defaultPlayersRed'
import defaultObjects from './../js/defaultObjects'
import initialHistory from '../js/history'
import { produce } from 'immer'

const initialState = {
    history: initialHistory,
    historyIndex: 1,
    panels: [
        {
            panelName: "Strategies",
            showPanel: false,
        },
        {
            panelName: "Draw",
            showPanel: false,
        },
        {
            panelName: "Players",
            showPanel: false,
        },
        {
            panelName: "Send",
            showPanel: false,
        },
        {
            panelName: "Objects",
            showPanel: false,
        },
        {
            panelName: "Mail",
            showPanel: false,
        },
        {
            panelName: "Config",
            showPanel: false,
        },
        {
            panelName: "User",
            showPanel: false,
        },
        {
            panelName: "Video",
            showPanel: false,
        },
        {
            panelName: "Menu",
            showPanel: false,
        },
    ],
    recicleByn: false,
    eraser: false,
    isDrawing: false,
    currentObjectSize: undefined,
    currentLineType: "Line 1",
    currentLineColor: "black",
    currentBackgroundPath: "https://res.cloudinary.com/leonemil/image/upload/",
    currentBackgroundAngle: "a_0",
    currentBackgroundCanvas: `/v1611525707/Synthesizer%20interface/background_lyevae.jpg`,
    currentUser: {
        name: undefined,
        email: undefined,
        avatar: undefined,
        strategies: [],
        videos: [],
        customTeams: [],
        otherData: []
    },
    configData: {
        bannerUrl: "",
        bannerAlt: "",
        logoUrl: ""
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.CHANGE_LINE_COLOR:
            return {
                ...state,
                currentLineColor: action.color,
            }

        case actions.CHANGE_LINE_TYPE:
            return {
                ...state,
                currentLineType: action.lineType
            }

        case actions.TOGGLE_DRAWING:
            return {
                ...state,
                isDrawing: action.boolean
            }

        case actions.CREATE_LINE:
            let newHistory = state.history.slice()
            let newLines = newHistory[state.historyIndex].lines.slice()
            newLines.splice(state.historyIndex + 1, newHistory.length - state.historyIndex + 1, action.line)
            let newHistoryObject = newHistory[state.historyIndex]
            const produceFunction = (newHistoryObject) => {
                return produce(newHistoryObject, (updated) => {
                    updated.lines = newLines
                })
            }
            newHistory.splice(state.historyIndex + 1, newHistory.length - state.historyIndex + 1, produceFunction(newHistoryObject))
            console.log(newHistory)
            return {
                ...state,
                history: newHistory,
                historyIndex: state.historyIndex + 1
            }

        case actions.SET_LINE:
            let newSetHistory = state.history.slice()
            newSetHistory[newSetHistory.length - 1] = produce(newSetHistory[newSetHistory.length - 1], updatedState => {
                updatedState.lines[updatedState.lines.length - 1].linePoints = [...updatedState.lines[updatedState.lines.length - 1].linePoints, action.x, action.y]
            })
            return {
                ...state,
                history: newSetHistory
            }

        case actions.ADD_STRATEGY:
            let currentUserWithNewStrategy = Object.assign({}, state.currentUser)
            currentUserWithNewStrategy.strategies.push(action.strategy)
            return {
                ...state,
                currentUser: currentUserWithNewStrategy
            }
        
        case actions.CLEAN_CANVAS:
            return {
                ...state,
                historyIndex: 0
            }

        case actions.SET_ERASER:
            return {
                ...state,
                eraser: action.boolean
            }

        case actions.TOGGLE_RECICLE_BYN:
            return {
                ...state,
                recicleByn: action.boolean
            }

        case actions.UNDO:
            let historyIndex = state.historyIndex
            historyIndex = historyIndex - 1
            if (historyIndex < 0) {
                historyIndex = historyIndex + 1
            }
            console.log(`Index: ${historyIndex}`)
            return {
                ...state,
                historyIndex: historyIndex
            }

        case actions.REDO:
            let historyLength = state.history.length
            let history = state.historyIndex
            history = history + 1
            if(history >= historyLength){
                history = history - 1
            }
            console.log(`Index: ${history}`)
            return {
                ...state,
                historyIndex: history
            }

        case actions.CHANGE_BACKGROUND:
            return {
                ...state,
                currentBackgroundCanvas: action.background
            }

        case actions.CHANGE_BACKGROUND_ANGLE:
            console.log(action.backgroundAngle)
            return {
                ...state,
                currentBackgroundAngle: action.backgroundAngle
            }

        case actions.TOGGLE_DEFAULT_PLAYERS_BLUE:
            let newDefaultPlayersBlue = state.history[state.historyIndex].defaultPlayersBlue.slice()
            newDefaultPlayersBlue[action.playersType] = newDefaultPlayersBlue[action.playersType].concat(defaultPlayersBlue[action.playersType].slice())
            let newHistoryWithPlayersBlue = state.history.slice()
            let newPlayersBlueHistory = produce(newHistoryWithPlayersBlue[state.historyIndex], playersBlueHistoryUpdated => {
                playersBlueHistoryUpdated.defaultPlayersBlue = newDefaultPlayersBlue
            })
            newHistoryWithPlayersBlue.splice(state.historyIndex + 1, newHistoryWithPlayersBlue.length - state.historyIndex + 1, newPlayersBlueHistory)

            return {
                ...state,
                history: newHistoryWithPlayersBlue,
                historyIndex: state.historyIndex + 1
            }

        case actions.TOGGLE_DEFAULT_PLAYERS_RED:
            let newDefaultPlayersRed = state.history[state.historyIndex].defaultPlayersRed.slice()
            newDefaultPlayersRed[action.playersType] = newDefaultPlayersRed[action.playersType].concat(defaultPlayersRed[action.playersType].slice())
            let newHistoryWithPlayersRed = state.history.slice()
            let newPlayersRedHistory = produce(newHistoryWithPlayersRed[state.historyIndex], playersRedHistoryUpdated => {
                playersRedHistoryUpdated.defaultPlayersRed = newDefaultPlayersRed
            })
            newHistoryWithPlayersRed.splice(state.historyIndex + 1, newHistoryWithPlayersRed.length - state.historyIndex + 1, newPlayersRedHistory)

            return {
                ...state,
                history: newHistoryWithPlayersRed,
                historyIndex: state.historyIndex + 1
            }

        case actions.TOGGLE_DEFAULT_OBJECTS:
            let newDefaultObjects = state.history[state.historyIndex].defaultObjects.slice()
            console.log(newDefaultObjects)
            newDefaultObjects[action.objectType] = newDefaultObjects[action.objectType].concat(defaultObjects[action.objectType].slice())
            let newHistoryWithObjects = state.history.slice()
            let newObjectHistory3 = produce(newHistoryWithObjects[state.historyIndex], objectHistoryUpdated => {
                objectHistoryUpdated.defaultObjects = newDefaultObjects
            })
            newHistoryWithObjects.splice(state.historyIndex + 1, newHistoryWithObjects.length - state.historyIndex + 1, newObjectHistory3)

            return {
                ...state,
                history: newHistoryWithObjects,
                historyIndex: state.historyIndex + 1
            }

        case actions.UPDATE_OBJECT_POSITION:
            let historyWithUpdatedPositions = state.history.slice()
            let lastHistoryObject = produce(historyWithUpdatedPositions[state.historyIndex], historyUpdated => {
                historyUpdated[action.objectType][action.typeId][action.elementId].position.x = action.x 
                historyUpdated[action.objectType][action.typeId][action.elementId].position.y = action.y 
            })
            console.log(state.historyIndex, historyWithUpdatedPositions.length - 1 - state.historyIndex, lastHistoryObject)
            historyWithUpdatedPositions.splice(state.historyIndex + 1, historyWithUpdatedPositions.length - state.historyIndex + 1, lastHistoryObject)
            console.log(historyWithUpdatedPositions)
            console.log(`Index: ${state.historyIndex + 1}`)
            return {
                ...state,
                history: historyWithUpdatedPositions,
                historyIndex: state.historyIndex + 1
            }

        case actions.SET_USER:
            return {
                ...state,
                currentUser: action.currentUser
            }

        case actions.SET_CONFIG_DATA:
            console.log(action.configData)
            return {
                ...state,
                configData: action.configData
            }
            
        case actions.TOGGLE_PANEL:
            //console.log(state.panels)
            let newPanels = state.panels.slice()
            newPanels.forEach((panel, id) => {
                if(id !== action.panel){
                    return (
                        panel.panelName,
                        panel.showPanel = false
                    )
                }
                else {
                    return (
                        panel.panelName,
                        panel.showPanel = panel.showPanel ? false : true
                    )
                }
            })
            console.log(newPanels)
            return {
                ...state,
                panels: newPanels
            }

        case actions.CHANGE_OBJECT_SIZE: 
            return {
                ...state,
                currentObjectSize: action.objectSize,
            }
            
    
        default: return state
    }
}

export default reducer