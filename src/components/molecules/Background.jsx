import React, { useState, useEffect } from 'react';
import { Image } from 'react-konva'
import useImage from 'use-image'

const Background = ({ backgroundPath, backgroundAngle, backgroundImage, height, width }) => {

    const [dimensions, setDimensions] = useState({
        width: window.innerWidth,
        height: window.innerHeight
    })

    useEffect(() => {
        window.addEventListener("resize", () => {
            if (dimensions.width !== window.innerWidth) {
                setDimensions({
                    width: window.innerWidth,
                    height: window.innerHeight
                })
            }
        })
    }, [dimensions.width])

    const [image] = useImage(`${backgroundPath}${backgroundAngle}${backgroundImage}`, 'Anonymous');
    const menuSize = width < 400 ? 30 : 60
    return (  
        <Image 
            image={image} 
            width={dimensions.width - menuSize} 
            height={dimensions.height} 
        />
    );
}
 
export default Background;