import React, { useState , useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faCog, faUserCircle, faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons'
//import './../js/hiddenMenu'

import MailPanel from '../Panels/MailPanel'
import ConfigPanel from '../Panels/ConfigPanel'
import UserPanel from '../Panels/UserPanel'

import { togglePanel } from './../../redux/actionCreators'
import { connect } from 'react-redux'

const HiddenMenu = ({ togglePanel, currentUser, configData }) => {


    // // ------------------------- Toggle panels logic ------------------------ //

    // const [panel, setPanel] = useState({
    //     currentPanel: "StrategyPanel",
    //     showPanel: false
    // })

    // useEffect(() => {
    //     togglePanel(panel)
    // })

    // const togglePanel = (panelData) => {

    //     if (panelData === panel.currentPanel && panel.showPanel === true) {
    //         setPanel({
    //             currentPanel: panel.currentPanel,
    //             showPanel: false
    //         })
    //         return;
    //     }
    //     if (panelData === panel.currentPanel && panel.showPanel === false) {
    //         setPanel({
    //             currentPanel: panel.currentPanel,
    //             showPanel: true
    //         })
    //         return;
    //     }
    //     switch (panelData) {

    //         case "MailPanel":
    //             setPanel({
    //                 currentPanel: "MailPanel",
    //                 showPanel: true
    //             })
    //             break;

    //         case "ConfigPanel":
    //             setPanel({
    //                 currentPanel: "ConfigPanel",
    //                 showPanel: true
    //             })
    //             break;

    //         case "UserPanel":
    //             setPanel({
    //                 currentPanel: "UserPanel",
    //                 showPanel: true
    //             })
    //             break;

    //         default:
    //             break;
    //     }
    // }

    // // -------------------------------------------------------------------//

    // -------------------------- Style of icons ----------------------- //

    let style = {
        width: "30px",
        height: "30px",
        cursor: "pointer",
        color: "white"
    }

    // ------------------------------------------------------------------ //

    // ------------------- Toggle hidden menu logic ------------------- //

    const [arrow, setArrow] = useState(faArrowLeft)
    const [isHidden, setIsHidden] = useState(true)
    const hiddenMenu = useRef()

    const toggleMenu = (e) => {
        console.log(hiddenMenu)
        e.stopPropagation()
        if (isHidden === true){
            setArrow(faArrowRight)
            hiddenMenu.current.style.transform = "scaleX(1)"
            hiddenMenu.current.style.width = "100%"
            setIsHidden(false)
        }
        else {
            setArrow(faArrowLeft)
            hiddenMenu.current.style.transform = "scaleX(0)"
            hiddenMenu.current.style.width = "0"
            setIsHidden(true)
        }
    }

    // --------------------------------------------------------------------- //

    return (
        <>
        <div className="hidden-menu__container" id="hiddenMenuContainer">
            {
                configData.logoUrl ?
                <div className="hidden-menu__logo">
                    <img src={configData.logoUrl} alt="Logo de gainberri" className="hidden-menu__logo-image"/>
                </div>
                : null
            }
            <div className="hidden-menu__button" id="hiddenMenuButton" onClick={toggleMenu}>
                <FontAwesomeIcon icon={arrow} style={style} />
            </div>
            <nav className="hidden-menu" ref={hiddenMenu}>
                <ul className="hidden-menu__list">
                    <li className="hidden-menu__item" data-tooltip-submenu="Enviar mensaje" onClick={() => { togglePanel(5) }}>
                        <FontAwesomeIcon icon={faEnvelope} style={style}/>
                    </li>
                    <li className="hidden-menu__item" data-tooltip-submenu="Ajustes" onClick={() => { togglePanel(6) }}>
                        <FontAwesomeIcon icon={faCog} style={style}/>
                    </li>
                    <li className="hidden-menu__item" data-tooltip-submenu="Nombre Usuario" onClick={() => { togglePanel(7) }}>
                        <FontAwesomeIcon icon={faUserCircle} style={style}/>
                    </li>
                </ul>
            </nav>
        </div>
        {
            currentUser ? 
                <>
                <MailPanel />
                <ConfigPanel /> 
                <UserPanel /> 
                </>
            :
            null
        }
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser,
        configData: state.configData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        togglePanel: (panel) => { dispatch(togglePanel(panel)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(HiddenMenu);