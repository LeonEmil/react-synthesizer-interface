import React, { useState, useRef } from 'react';
import { Group, Circle, Rect, Text, Image, Transformer } from 'react-konva'
import useImage from 'use-image';

const DefaultPlayersRed = ({ history, historyIndex, currentObjectSize, width, height, updateObjectPosition, mouseX, mouseY, eraser }) => {

    const [player1] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549510/Handball%20manager/Player-red-1_bmexdy.png', 'Anonymous')
    const [player2] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549512/Handball%20manager/player-red-2_lpiwfe.png', 'Anonymous')
    const [player3] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1602549512/Handball%20manager/player-red-3_suquyh.png', 'Anonymous')
    const [player4] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549514/Handball%20manager/player-red-4_gqchxz.png', 'Anonymous')
    const [player5] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549514/Handball%20manager/player-red-5_tbmqej.png', 'Anonymous')
    const [player6] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549515/Handball%20manager/player-red-6_jurf2s.png', 'Anonymous')
    const [player7] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549513/Handball%20manager/player-red-7_cz1i3f.png', 'Anonymous')
    const [player8] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549508/Handball%20manager/player-red-8_tpk9am.png', 'Anonymous')
    const [recicleBynImage] = useImage('https://www.practicepanther.com/wp-content/uploads/2016/07/recycle-bin.png', 'Anonymous')

    const playerRef = useRef()
    const transformRef = useRef()

    const [recicleByn, setRecicleByn] = useState(false)

    const size = currentObjectSize === "normal" ? 60 :
                 currentObjectSize === "medium" ? 45 :
                 currentObjectSize === "small" ? 30 :
                 width < 900 || height < 600 ? 30 : 60

    const fontSize = size === 60 ? 30 :
            size === 45 ? 24 :
            size === 30 ? 17 :
            null

    const textPadding = currentObjectSize === "big" ? 21 :
                        currentObjectSize === "medium" ? 17 :
                        currentObjectSize === "small" ? 10 :
                        width < 900 || height < 600 ? 10 : 21

    const toggleTransformBox = () => {
        if (transformRef.current.getNode()) {
            transformRef.current.nodes([]);
            transformRef.current.getLayer().batchDraw();
        }
        else {
            transformRef.current.nodes([playerRef.current]);
            transformRef.current.getLayer().batchDraw();
        }
    }

    return (  
        <>
            {
                history[historyIndex].defaultPlayersRed[0].map((player, key) => (
                    <Group 
                        key={key} 
                        draggable
                        x={player.position.x}
                        y={player.position.y}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultPlayersRed', 0, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    >
                        <Image
                            image={player1}
                            width={size}
                            height={size}
                        />
                        <Text
                            text={key + 1}
                            x={textPadding}
                            y={textPadding}
                            fontSize={fontSize}
                            fill="white"
                        />
                    </Group>
                ))
            }
            {
                history[historyIndex].defaultPlayersRed[1].map((player, key) => (
                    <Group 
                        key={key} 
                        draggable
                        x={player.position.x}
                        y={player.position.y}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultPlayersRed', 1, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    >
                        <Image
                            image={player2}
                            width={size}
                            height={size}
                        />
                        <Text
                            text={key + 1}
                            x={textPadding}
                            y={textPadding}
                            fontSize={fontSize}
                            fill="white"
                        />
                    </Group>
                ))
            }
            {
                history[historyIndex].defaultPlayersRed[2].map((player, key) => (
                    <Group 
                        key={key} 
                        draggable
                        x={player.position.x}
                        y={player.position.y}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultPlayersRed', 2, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    >
                        <Image
                            image={player3}
                            width={size}
                            height={size}
                        />
                        <Text
                            text={key + 1}
                            x={textPadding}
                            y={textPadding}
                            fontSize={fontSize}
                            fill="white"
                        />
                    </Group>
                ))
            }
            {
                history[historyIndex].defaultPlayersRed[3].map((player, key) => (
                    <Image
                        key={key}
                        image={player4}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        //rotation={Math.atan2(window.innerHeight / 2 - player.position.y, 10 - player.position.x) * 57.2957795}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultPlayersRed', 3, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                history[historyIndex].defaultPlayersRed[4].map((player, key) => (
                    <Image
                        key={key}
                        image={player5}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        //rotation={Math.atan2(window.innerHeight / 2 - player.position.y, 10 - player.position.x) * 57.2957795}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultPlayersRed', 4, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                history[historyIndex].defaultPlayersRed[5].map((player, key) => (
                    <Image
                        key={key}
                        image={player6}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        //rotation={Math.atan2(window.innerHeight / 2 - player.position.y, 10 - player.position.x) * 57.2957795}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultPlayersRed', 5, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                history[historyIndex].defaultPlayersRed[6].map((player, key) => (
                    <Image
                        key={key}
                        image={player7}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        //rotation={Math.atan2(window.innerHeight / 2 - player.position.y, 10 - player.position.x) * 57.2957795}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultPlayersRed', 6, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                history[historyIndex].defaultPlayersRed[7].map((player, key) => (
                    <Image
                        key={key}
                        image={player8}
                        x={player.position.x}
                        y={player.position.y}
                        width={size}
                        height={size}
                        draggable
                        offsetX={20}
                        offsetY={20}
                        //rotation={Math.atan2(window.innerHeight / 2 - player.position.y, 10 - player.position.x) * 57.2957795}
                        onDragStart={() => {
                            if (eraser) {
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultPlayersRed', 7, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if(eraser && recicleBynPosition){
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            <Transformer
                ref={transformRef}
                centeredScaling={false}
                resizeEnabled={false}
                rotationSnapTolerance={10}
                rotationSnaps={[0, 90, 180, 270]}
                borderStroke="black"
            >
            </Transformer>
            {
                recicleByn ?
                    <>
                        <Rect
                            x={0}
                            y={0}
                            width={window.innerWidth}
                            height={window.innerHeight}
                            fill="hsla(0, 0%, 0%, 0.5)"
                        >
                        </Rect>
                        <Circle
                            radius={200}
                            x={0}
                            y={(window.innerHeight / 2)}
                            fill="hsla(0, 100%, 50%, 0.5)"
                        />
                    </>
                    : null
            }
        </>
    );
}

export default DefaultPlayersRed;