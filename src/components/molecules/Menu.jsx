import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faHome, faDoorOpen, faSave, faFolderOpen, faVideo, faUsers, faEdit, faTrashAlt, faSync, faUndo, faRedo, faStar, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux'
import { togglePanel, undo, redo } from './../../redux/actionCreators'
import { cleanCanvas, addStrategy, changeLineColor, toggleRecicleByn } from './../../redux/actionCreators'
import { storage } from "./../../firebase/firebase"
import { auth, db } from './../../firebase/firebase'

//import LeftPanel from './Left panel/LeftPanel'
import MenuPanel from '../Panels/MenuPanel'
import StrategyPanel from '../Panels/StrategyPanel'
import DrawPanel from '../Panels/DrawPanel'
import PlayersPanel from '../Panels/PlayersPanel'
//import SendPanel from '../Panels/SendPanel'
import ObjectsPanel from '../Panels/ObjectsPanel'
import VideoPanel from '../Panels/VideoPanel'
import MailPanel from '../Panels/MailPanel'
import UserPanel from '../Panels/UserPanel'
import ConfigPanel from '../Panels/ConfigPanel'
import { setEraser } from './../../redux/actionCreators'

const Menu = ({cleanCanvas, undo, redo, togglePanel, addStrategy, currentUser, setEraser, canvasWidth, canvasHeight, recicleByn, toggleRecicleByn }) => {

    const menu = useRef()
    const recicleBynRef = useRef()

    let style = 
    canvasHeight < 700 || canvasWidth < 400 ?
        {
            width: "15px",
            height: "15px",
            cursor: "pointer",
            color : "white"
        } 
    :
        {
            width: "30px",
            height: "30px",
            cursor: "pointer",
            color : "white"
        }

    const saveImage = () => {
        const canvas = document.querySelectorAll("canvas")[0]
        const canvasLines = document.querySelectorAll("canvas")[1]
        const canvasPlayers = document.querySelectorAll("canvas")[2]
        canvas.getContext("2d").drawImage(canvasLines, 0, 0)
        canvas.getContext("2d").drawImage(canvasPlayers, 0, 0)
        const storageRef = storage.ref()
        const fileName = window.prompt("Nombre de la estrategia")
        if(fileName !== null){
            canvas.toBlob(blob => {
                var image = new Image();
                image.src = blob;
                storageRef.child(`${currentUser.name}/images/${fileName}`).put(blob)
                    .then(async () => {
                        let url = await storageRef.child(`${currentUser.name}/images/${fileName}`).getDownloadURL()
                        let metadata = await storageRef.child(`${currentUser.name}/images/${fileName}`).getMetadata()
                        db.collection('users').where('email', '==', currentUser.email).get().then(response => {
                            let id = response.docs[0].id
                            let doc = response.docs[0].data()
                            doc.strategies.push({
                                name: fileName,
                                url: url,
                                size: `${Math.floor(metadata.size / 1000)} Kb`,
                                date: metadata.timeCreated.slice(0, 10)
                            })
                            db.collection('users').doc(id).update(doc).then(() => {alert("Estrategia guardada")})
                        })
                    })
                    .catch(error => {
                        console.log(error)
                    });
            }); 
        }
    }

    const turnOffRecicleByn = () => {
        toggleRecicleByn(false); 
        recicleBynRef.current.style.backgroundColor = "#085e5f"; 
        document.body.style.cursor = "default"; 
        setEraser(false)
    }

    return (
        <>
        {
            <nav className="menu" ref={menu}>
                <ul className="menu__list">
                    <li className="menu__item" data-tooltip-menu="Salir" onClick={() => { auth.signOut(); turnOffRecicleByn()} }>
                        <FontAwesomeIcon icon={faDoorOpen} size="lg" color="white" style={style} />
                    </li>
                    <li className="menu__item" data-tooltip-menu="Guardar" onClick={() => { saveImage(); turnOffRecicleByn() }}>
                        <div className="menu__icon-save"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Mis estrategias" onClick={() => { togglePanel(0); turnOffRecicleByn() }}>
                        <div className="menu__icon-folder"></div>
                    </li>
                        {/* <li className="menu__item" data-tooltip-menu="Grabar" onClick={() => { togglePanel(8) }}>
                            <FontAwesomeIcon icon={faVideo} size="lg" color="white" style={style} />
                        </li> */}
                    <li className="menu__item" data-tooltip-menu="Trazar movimientos" onClick={() => { togglePanel(1); turnOffRecicleByn() }}>
                        <div className="menu__icon-edit"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Limpiar cancha" onClick={() => { cleanCanvas(); turnOffRecicleByn() }}>
                        <div className="menu__icon-refresh"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Deshacer" onClick={() => { undo(); turnOffRecicleByn() }}>
                        <div className="menu__icon-undo"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Rehacer" onClick={() => { redo(); turnOffRecicleByn() }}>
                        <div className="menu__icon-redo"></div>
                    </li>
                    <li className="menu__item" data-tooltip-menu="Jugadores" onClick={() => { togglePanel(2); turnOffRecicleByn() }}>
                        <div className="menu__icon-players"></div>
                    </li>
                        {/* <li className="menu__item" onClick={() => { togglePanel(3) }}>
                        <FontAwesomeIcon icon={faSignOutAlt} size="lg" color="white" style={style} rotation={270}/>
                    </li> */}
                    <li className="menu__item" data-tooltip-menu="Entrenamientos" onClick={() => { togglePanel(4); turnOffRecicleByn() }}>
                        <div className="menu__icon-star"></div>
                    </li>
                    <li className="menu__item" ref={recicleBynRef} data-tooltip-menu="Borrar elementos" onClick={() => { 
                        
                        if(recicleByn){
                            recicleBynRef.current.style.backgroundColor = "#085e5f"
                            document.body.style.cursor = "default"
                            toggleRecicleByn(false)
                            setEraser(false)
                        }
                        else {
                            recicleBynRef.current.style.backgroundColor = "red"
                            document.body.style.cursor = "move"
                            toggleRecicleByn(true)
                            setEraser(true)
                            window.alert("Arrastre elementos o lineas a la zona de la papelera para borrarlos")
                        }
                    }}>
                        <FontAwesomeIcon icon={faTrashAlt} size="lg" color="white" style={style} />
                    </li>
                </ul>
            </nav>
        }
            {
                currentUser ?
                    <>
                        <StrategyPanel menuRef={menu}/> 
                        <MenuPanel menuRef={menu}/>
                        <DrawPanel menuRef={menu}/> 
                    </>
                :
                null
            }
            <PlayersPanel menuRef={menu}/> 
            <ObjectsPanel menuRef={menu}/> 
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser,
        recicleByn: state.recicleByn
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        undo: () => { dispatch(undo()) },
        redo: () => { dispatch(redo()) },
        cleanCanvas: () => { dispatch(cleanCanvas()) },
        togglePanel: (panel) => { dispatch(togglePanel(panel)) },
        addStrategy: (strategy) => { dispatch(addStrategy(strategy)) },
        changeLineColor: (color) => { dispatch(changeLineColor(color)) },
        setEraser: (boolean) => { dispatch(setEraser(boolean)) },
        toggleRecicleByn: (boolean) => { dispatch(toggleRecicleByn(boolean)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(Menu);