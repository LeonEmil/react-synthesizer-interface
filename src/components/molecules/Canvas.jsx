import React, { useState, useEffect, useRef } from 'react';
import { Stage, Layer, Group, Shape, Line, Rect, Circle, Image } from 'react-konva';
import Background from './Background'
import useImage from 'use-image' 
import { storage } from './../../firebase/firebase'

import { connect } from 'react-redux'
import DefaultPlayersBlue from './DefaultPlayersBlue';
import DefaultPlayersRed from './DefaultPlayersRed';
import DefaultObjects from './DefaultObjects';
import { createLine, setLine, toggleDrawing, updateObjectPosition } from './../../redux/actionCreators'
//import { drawOn } from "./Menu"


const Canvas = (props) => {

    const [image] = useImage(`https://res.cloudinary.com/leonemil/image/upload/v1602549539/Handball%20manager/preview-2_ecdmqo.jpg`, 'Anonymous')
    const [recicleBynImage] = useImage('https://www.practicepanther.com/wp-content/uploads/2016/07/recycle-bin.png', 'Anonymous')
    
    //console.log(image)

    const [recicleByn, setRecicleByn] = useState(false)

    const [pattern, setPattern] = useState()
    const stageRef = useRef()

    useEffect(() => {
        if(image){
            image.width = window.innerWidth
            image.height = window.innerHeight
        }
        //console.log(image)
    },[image])

    // componentDidUpdate(){
    //     const canvas = document.querySelectorAll('canvas')
    //     console.log(canvas[1])
    //     const context = canvas[1].getContext("2d")
    //     const image = new Image()
    //     image.src = 'https://res.cloudinary.com/leonemil/image/upload/v1602549537/Handball%20manager/preview-5_ylxkus.jpg'
    //     image.width = window.innerWidth
    //     image.height = window.innerHeight
    //     var pattern = context.createPattern(image, 'repeat');
    //     context.rect(0, 0, window.innerWidth, window.innerHeight);
    //     context.fillStyle = "red";
    //     context.fill(); 
    //     console.log(pattern)
    //     setPattern(pattern)
    // }

    // componentDidUpdate(){
    //     const canvas = document.querySelectorAll('canvas')
    //     const c = canvas[1].getContext("2d")
    //     c.fillRect(100, 100, 400, 400)
    //     c.fill()
    // }

    const arrowSize = props.canvasWidth < 900 || props.canvasHeight < 600 ? 10 : 20
    return (
        <>
        <Stage
            width={props.canvasWidth}
            height={props.canvasHeight}  
            ref={stageRef}
            // onMouseMove={(e) => {
            //     const stage = stageRef.getStage();
            //     const point = stage.getPointerPosition();
            //     setState({
            //         mouseX: point.x,
            //         mouseY: point.y
            //     })
            //     //console.log(state)
            // }}
        >
            <Layer>
                <Background
                    backgroundPath={props.currentBackgroundPath}
                    backgroundAngle={props.currentBackgroundAngle}
                    backgroundImage={props.currentBackgroundCanvas}
                    height={window.innerWidth}
                    width={window.innerHeight}
                />  
            </Layer>
            <Layer
                // onClick={() => {
                //     // const canvas = document.querySelectorAll('canvas')[0]
                //     // const context = canvas.getContext("2d")
                //     // const storageRef = storage.ref()
                //     //     canvas.toBlob(blob => {
                //     //         var image = new Image();
                //     //         image.src = blob;
                //     //         const fileName = "BackgroundPattern"
                //     //         storageRef.child(`${props.currentUser.name}/images/${fileName}`).put(blob)
                //     //             .then(async () => {
                //     //                 let url = await storageRef.child(`${props.currentUser.name}/images/${fileName}`).getDownloadURL()
                //     //                 //var pattern = context.createPattern(url, 'repeat');
                //     //                 //setPattern(pattern)
                //     //                 console.log(url)
                //     //                 //console.log(pattern)
                //     //             })
                //     //             .catch(error => {
                //     //                 console.log(error)
                //     //             });
                //     //     });
                    
                    

                //     //console.log(canvas[1])
                //     //const image = new Image()
                //     //image.src = 'https://res.cloudinary.com/leonemil/image/upload/v1602549537/Handball%20manager/preview-5_ylxkus.jpg'
                //     //image.width = `${window.innerWidth}px`
                //     //image.height = `${window.innerHeight}px`
                //     //console.log(image)
                // //    context.rect(0, 0, window.innerWidth, window.innerHeight);
                // //    context.fillStyle = "red";
                // //    context.fill(); 
                // //    console.log(pattern)
                // }}
            >
                <Background 
                    backgroundPath={props.currentBackgroundPath}
                    backgroundAngle={props.currentBackgroundAngle}
                    backgroundImage={props.currentBackgroundCanvas}
                    height={window.innerWidth}
                    width={window.innerHeight}
                />                                     
            </Layer>
            <Layer>
                <DefaultObjects 
                    history={props.history}
                    historyIndex={props.historyIndex}
                    toggleDrawing={props.toggleDrawing} 
                    eraser={props.eraser}
                    width={props.canvasWidth} 
                    height={props.canvasHeight}
                    currentObjectSize={props.currentObjectSize}
                    updateObjectPosition={props.updateObjectPosition}
                    recicleByn={props.recicleByn}
                />
            </Layer>
        </Stage>
        </>
    )
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        history: state.history,
        historyIndex: state.historyIndex,
        currentObjectSize: state.currentObjectSize,
        currentLineColor: state.currentLineColor,
        currentLineType: state.currentLineType,
        currentBackgroundPath: state.currentBackgroundPath,
        currentBackgroundAngle: state.currentBackgroundAngle,
        currentBackgroundCanvas: state.currentBackgroundCanvas,
        isDrawing: state.isDrawing,
        eraser: state.eraser,
        recicleByn: state.recicleByn
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createLine: (currentLineColor, currentLineType) => { dispatch(createLine(currentLineColor, currentLineType)) },
        setLine: (x, y) => { dispatch(setLine(x, y)) },
        toggleDrawing: (boolean) => { dispatch(toggleDrawing(boolean)) },
        updateObjectPosition: (objectType, typeId, elementId, x, y) => { dispatch(updateObjectPosition(objectType, typeId, elementId, x, y)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Canvas)