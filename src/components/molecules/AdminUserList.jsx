import React, { useState, useEffect, Fragment } from 'react';
import { db, auth } from '../../firebase/firebase'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

const AdminUserList = ({ currentUser }) => {
    
    const { handleSubmit, handleChange, handleBlur, values, touched, errors } = useFormik({
        initialValues: {
            name: '',
            email: '',
            role: ''
        },
        validationSchema: Yup.object({
            name: Yup.string().max(10, 'El nombre de usuario debe ser más corto de 10 caracteres').required('Ingrese un nombre de usuario'),
            email: Yup.string().email().required('Por favor ingrese el email de un usuario')
        }),
        onSubmit: ({ name, email, role }) => {
            console.log(name, email, role)
            db.collection('users').where('email', '==', email).get().then(response => {
                console.log(response)
                const id = response.docs[0].id
                db.collection('users').doc(id).update({
                    name: name,
                    role: role
                })
                .then(() => {
                    alert('Usuario actualizado')
                })
                .catch(error => {
                    alert(error)
                })
            })
        }
    })

    const [displayModal, setDisplayModal] = useState(false)
    const [emailList, setEmailList] = useState([])

    const [users, setUsers] = useState([])

    useEffect(() => {
        db.collection('users').get().then(users => {
            let usersList = []
            users.docs.forEach(user => {
                usersList.push(user.data())
            })
            setUsers(usersList)
        })
    }, [users, setUsers])
    
    return (  
        <>
        <table>
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Rol</th>
                </tr>
            </thead>
            <tbody>
            {
                users.map((user, key) => {
                    return (
                        <tr key={key}>
                            <td>{user.name}</td>
                            <td>{user.email}</td>
                            <td>{user.role}</td>
                        </tr>
                    )
                })
            }
            </tbody>
        </table>
        <div className="admin__modal" style={displayModal ? {transform: 'scaleY(1)'} : {transform: 'scaleY(0)'}}>
            <div className="admin__exit-container" onClick={() => { setDisplayModal(false) }}>
                <FontAwesomeIcon icon={faTimes} color="#fff" size="2x" className="draw-panel__exit-item" />
            </div>
            <form onSubmit={handleSubmit} className="admin__form">
                <label htmlFor="email">Escriba el email de usuario que quiere actualizar</label>
                <input 
                    type="email" 
                    name="email" 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    className="admin__form-input"
                />
                {
                    errors.email && touched.email ?
                        <span className="login__alert">{errors.email}</span>
                        : null
                }
                <label htmlFor="name">Nuevo nombre de usuario</label>
                <input 
                    type="text" 
                    name="name" 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.name}
                    className="admin__form-input"
                />
                {
                    currentUser.role === "superadmin" ?
                    <>
                    <label htmlFor="role">Nuevo rol de usuario (user, admin o superadmin)</label>
                    <input 
                        type="text"
                        name="role"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.role}
                        className="admin__form-input"
                    />
                    </>
                    :
                    null
                }
                <input type="submit" value="Actualizar"/>
            </form>
        </div>
        <button onClick={() => { setDisplayModal(true) }}>Editar usuarios</button>
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser
    }
}
 
export default connect(mapStateToProps, null)(AdminUserList);