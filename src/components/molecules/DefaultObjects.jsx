import React, { useState, useRef } from 'react';
import { Group, Image, Transformer, Rect, Circle, Text } from 'react-konva'
import useImage from 'use-image'

const DefaultObjects = ({ history, historyIndex, currentObjectSize, width, height, updateObjectPosition, eraser }) => {

    const [object1] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1611525700/Synthesizer%20interface/button-1_zrsi8v.png', 'Anonymous')
    const [object2] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1611525701/Synthesizer%20interface/button-2_ogr3jv.png', 'Anonymous')
    const [object3] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1611525702/Synthesizer%20interface/button-4_l0swul.png', 'Anonymous')
    const [object4] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1611525701/Synthesizer%20interface/button-5_vvfd7o.png', 'Anonymous')
    const [object5] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1611525701/Synthesizer%20interface/button-7_jxftvw.png', 'Anonymous')
    const [object6] = useImage('https://res.cloudinary.com/leonemil/image/upload/v1611525702/Synthesizer%20interface/button-8_di8goq.png', 'Anonymous')

    const size = currentObjectSize === "normal" ? 0.70 :
                 currentObjectSize === "medium" ? 0.50 :
                 currentObjectSize === "small" ? 0.4 :
                 width < 900 || height < 600 ? 0.2 : 0.6

    const fontSize = size === 60 ? 30 :
        size === 45 ? 24 :
            size === 30 ? 17 :
                null

    const textPadding = currentObjectSize === "big" ? 21 :
        currentObjectSize === "medium" ? 17 :
            currentObjectSize === "small" ? 10 :
                width < 900 || height < 600 ? 10 : 21

    const playerRef = useRef()
    const transformRef = useRef()

    const toggleTransformBox = () => {
        if (transformRef.current.getNode()) {
            transformRef.current.nodes([]);
            transformRef.current.getLayer().batchDraw();
        }
        else {
            transformRef.current.nodes([playerRef.current]);
            transformRef.current.getLayer().batchDraw();
        }
    }

    const [recicleByn, setRecicleByn] = useState(false)

    return (
        <>
            {
                history[historyIndex].defaultObjects[0].map((object, key) => (
                    <Image
                        key={key}
                        image={object1}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultObjects', 0, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                history[historyIndex].defaultObjects[1].map((object, key) => (
                    <Image
                        key={key}
                        image={object2}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultObjects', 1, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                history[historyIndex].defaultObjects[2].map((object, key) => (
                    <Image
                        key={key}
                        image={object3}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultObjects', 2, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                history[historyIndex].defaultObjects[3].map((object, key) => (
                    <Image
                        key={key}
                        image={object4}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultObjects', 3, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                history[historyIndex].defaultObjects[4].map((object, key) => (
                    <Image
                        key={key}
                        image={object5}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultObjects', 4, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            {
                history[historyIndex].defaultObjects[5].map((object, key) => (
                    <Image
                        key={key}
                        image={object6}
                        x={object.position.x}
                        y={object.position.y}
                        scaleX={size + 0.10}
                        scaleY={size + 0.10}
                        draggable 
                        onDragStart={() => {
                            if(eraser){
                                setRecicleByn(true)
                            }
                        }}
                        onDragEnd={(e) => {
                            updateObjectPosition('defaultObjects', 5, key, e.target.x(), e.target.y())
                            let recicleBynPosition = e.target.x() < 200 && e.target.x() > -200 &&
                                                 e.target.y() < (window.innerHeight / 2) + 200 && e.target.y() > (window.innerHeight / 2) - 200 ? true : false
                            if (eraser && recicleBynPosition) {
                                e.target.setAttr('x', 10000)
                            }
                            setRecicleByn(false)
                        }}
                        onClick={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                        onTap={(e) => {
                            playerRef.current = e.currentTarget
                            toggleTransformBox()
                        }}
                    />
                ))
            }
            <Transformer
                ref={transformRef}
                centeredScaling={false}
                resizeEnabled={true}
                rotationSnapTolerance={10}
                rotationSnaps={[0, 90, 180, 270]}
                borderStroke="blue"
            >
            </Transformer>
            {
                recicleByn ?
                    <>
                        <Rect
                            x={0}
                            y={0}
                            width={window.innerWidth}
                            height={window.innerHeight}
                            fill="hsla(0, 0%, 0%, 0.5)"
                        >
                        </Rect>
                        <Circle
                            radius={200}
                            x={0}
                            y={(window.innerHeight / 2)}
                            fill="hsla(0, 100%, 50%, 0.5)"
                        />
                    </>
                    : null
            }
        </>
    );
}

export default DefaultObjects;