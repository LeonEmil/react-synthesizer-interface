import React from 'react';
import { Image, Group, Text } from 'react-konva'
import useImage from 'use-image'

const CustomPlayers = ({ history, historyIndex }) => {

    const [player1] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549511/Handball%20manager/player-blue-1_j6ofta.png', 'Anonymous')
    const [player2] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549509/Handball%20manager/player-blue-2_v0q2iu.png', 'Anonymous')
    const [player3] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549507/Handball%20manager/player-blue-3_fibk72.png', 'Anonymous')
    const [player4] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549507/Handball%20manager/player-blue-4_xo9bse.png', 'Anonymous')
    const [player5] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549511/Handball%20manager/player-blue-5_qsjpyh.png', 'Anonymous')
    const [player6] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549507/Handball%20manager/player-blue-6_nvzx0g.png', 'Anonymous')
    const [player7] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549509/Handball%20manager/player-blue-7_qwycfi.png', 'Anonymous')
    const [player8] = useImage('https://res.cloudinary.com/leonemil/image/upload/a_90/v1602549510/Handball%20manager/player-blue-8_y7w3kk.png', 'Anonymous')

    return (
        <>
            {
                history[historyIndex].customPlayers.players[0].map((player, key) => (
                    <Group key={key} draggable>
                        <Image
                            image={player1}
                            x={player.history[0].x}
                            y={player.history[0].y}
                            width={60}
                            height={60}
                        />
                        <Text
                            text={player.number}
                            x={player.history[0].x + 23}
                            y={player.history[0].y + 17}
                            fontSize={30}
                            fill="white"
                        />
                    </Group>
                ))
            }
            {
                history[historyIndex].customPlayers.players[1].map((player, key) => (
                    <Group key={key} draggable>
                        <Image
                            image={player2}
                            x={player.history[0].x}
                            y={player.history[0].y}
                            width={60}
                            height={60}

                        />
                        <Text
                            text={player.number}
                            x={player.history[0].x + 23}
                            y={player.history[0].y + 17}
                            fontSize={30}
                            fill="white"
                        />
                    </Group>
                ))
            }
        </>
    );
}


export default CustomPlayers;