import React from 'react';
import Menu from './../molecules/Menu'
import HiddenMenu from './../molecules/HiddenMenu'
import Canvas from './../molecules/Canvas'

const Home = (props) => {
    return (  
        <>
            <Canvas canvasWidth={props.canvasWidth} canvasHeight={props.canvasHeight}/>
        </>
    );
}
 
export default Home;