import React, { useState, useEffect } from 'react';
import { Redirect, Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faVolleyballBall, faDoorOpen, faSave, faFolderOpen, faVideo, faUsers, faUserPlus, faImages, faSync, faUndo, faRedo, faStar, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux'
import { db, auth } from '../../firebase/firebase'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import MenuAdmin from './../molecules/MenuAdmin'
import AdminUserList from './../molecules/AdminUserList'

const Register = ({ canvasWidth, canvasHeight, currentUser }) => {

    const [tab, setTab] = useState("Create user")

    let style =
        canvasHeight < 700 || canvasWidth < 400 ?
            {
                width: "15px",
                height: "15px",
                cursor: "pointer",
                color: "white"
            }
            :
            {
                width: "30px",
                height: "30px",
                cursor: "pointer",
                color: "white"
            }

    const {handleSubmit, handleChange, handleBlur,values, touched, errors} = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
            confirmPassword: ''
        },
        validationSchema: Yup.object({
            name: Yup.string().max(10, 'Su nombre de usuario debe ser más corto de 10 caracteres').required('Ingrese su nombre de usuario'),
            email: Yup.string().required().email().required('Ingrese su email'),
            password: Yup.string().min(6, 'La contraseña debe tener como mínimo 6 caracteres').required('Ingrese su contraseña'),
            confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Las contraseñas deben coincidir').required('Ingrese su contraseña nuevamente')
        }),
        onSubmit: ({name, email, password}) => {
            auth.createUserWithEmailAndPassword(email, password)
                .then(() => {
                    db.collection('users').add({
                        name: `${name}`,
                        email: `${email}`,
                        avatar: 'https://res.cloudinary.com/leonemil/image/upload/v1602549579/Handball%20manager/user-image_mvzyz8.jpg',
                        customTeams: [],
                        strategies: [],
                        videos: [],
                        otherData: [],
                        role: "user"
                    }).then(() => {
                        alert("Usuario creado")
                    })
                })
                .catch(error => {
                    alert(error)
                })
        }
    })

    const handleSubmitImages = (e) => {
        e.preventDefault()
        e.persist()
        db.collection('data').doc('n2lXFZcC0p9aZ4y4BMCE').update({
            bannerAlt: e.target[2].value,
            bannerUrl: e.target[1].value,
            logoUrl: e.target[0].value
        })
        .then(() => {
            alert("Datos actualizados")
        })
        .catch(error => {
            alert(error)
        })
    }

return currentUser.role === "user" ? ( 
            <div className="register-page">
                <h1>Usted no tiene permisos para ver esta página</h1>
            </div>
        )
        :
        (
            <>
                <nav className="menu">
                    <ul className="menu-admin__list">
                        <li className="menu-admin__item" onClick={() => { setTab("Create user") }}>
                            <FontAwesomeIcon icon={faUserPlus} size="lg" color="white" style={style} />
                            {canvasWidth > 900 ? <span className="menu__span">Crear usuario</span> : null}
                        </li>
                        <li className="menu-admin__item" onClick={() => { setTab("Users list") }}>
                            <FontAwesomeIcon icon={faUsers} size="lg" color="white" style={style} />
                            { canvasWidth > 900 ? <span className="menu__span">Lista de usuarios creados</span> : null }
                            
                        </li>
                        <li className="menu-admin__item" onClick={() => { setTab("Update images") }}>
                            <FontAwesomeIcon icon={faImages} size="lg" color="white" style={style} />
                            { canvasWidth > 900 ? <span className="menu__span">Configuración de imágenes</span> : null }
                            
                        </li>
                        <li className="menu-admin__item">
                            <Link to="/" className="menu-admin__item">
                                <FontAwesomeIcon icon={faVolleyballBall} size="lg" color="white" style={style} />
                                { canvasWidth > 900 ? <span className="menu__span">Ir a la aplicación</span> : null }
                            </Link>
                        </li>
                        <li className="menu-admin__item" onClick={() => { auth.signOut() }}>
                            <FontAwesomeIcon icon={faDoorOpen} size="lg" color="white" style={style} />
                            { canvasWidth > 900 ? <span className="menu__span">Cerrar sesión</span> : null }
                        </li>
                    </ul>
                </nav>
                <div className="register-page">
                    <Link to="/">
                        <div className="form-image"></div>
                    </Link>
                    {
                        tab === "Create user" ?
                        <form className="login__form" onSubmit={handleSubmit}>
                            <h1 className="login__title">Crear nuevo usuario</h1>
                            
                            <label htmlFor="register-name" className="login__label">Nombre de usuario</label>
                            <input 
                                value={values.name}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className="login__input" 
                                id="register-name"
                                name="name"
                                type="text" 
                                required
                            />
                            { 
                                touched.name && errors.name ? 
                                <span className="login__alert">{errors.name}</span> 
                                : null 
                            }
                    
                            <label htmlFor="register-email" className="login__label">Email</label>
                            <input 
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className="login__input" 
                                id="register-email"
                                name="email"
                                type="email"
                                required 
                            />
                            { 
                                touched.email && errors.email ? 
                                <span className="login__alert">{errors.email}</span> 
                                : null
                            }
        
                            <label htmlFor="register-password" className="login__label">Contraseña</label>
                            <input 
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className="login__input" 
                                value={values.password}
                                id="register-password"
                                name="password"
                                type="password" 
                                required
                            />
                            { 
                                touched.password && errors.password ? 
                                <span className="login__alert">{errors.password}</span> 
                                : null
                            }
        
                            <label htmlFor="register-confirm-password" className="login__label">Confirmar contraseña</label>
                            <input 
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.confirmPassword}            
                                className="login__input" 
                                id="register-confirm-password" 
                                name="confirmPassword"
                                type="password"
                                required
                            />
                            { 
                                touched.confirmPassword && errors.confirmPassword ? 
                                <span className="login__alert">{errors.confirmPassword}</span> 
                                : null
                            }
        
                            <input type="submit" value="Crear cuenta" className="login__button"/>
                        </form>
        
                        : tab === "Update images" ?
                        <form className="login__form" onSubmit={handleSubmitImages}>
                            <h1 className="login__title">Configuración de imágenes</h1>
        
                            <label htmlFor="register-password" className="login__label">Url del logo</label>
                            <input 
                                className="login__input" 
                                id="register-password"
                                name="logoUrl"
                                type="text" 
                            />
        
                            <label htmlFor="register-name" className="login__label">Url del banner</label>
                            <input 
                                className="login__input" 
                                id="register-name"
                                name="bannerUrl"
                                type="text" 
                            />
                            
                            <label htmlFor="register-email" className="login__label">Texto alternativo del banner</label>
                            <input 
                                className="login__input" 
                                id="register-email"
                                name="bannerAlt"
                                type="text"
                            />
                            
                            <input type="submit" value="Actualizar" className="login__button"/>
                        </form>
        
                        : tab === "Users list" ? 
                            <AdminUserList />
                        : null
                    }
                </div>
            </>
        )
}

export default connect(null, null)(Register);
