import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import NotFound from './molecules/NotFound'
import './../sass/styles.scss'
import { connect } from 'react-redux'
import { auth, db } from './../firebase/firebase'
import { setUser, changeBackgroundAngle, setConfigData } from './../redux/actionCreators'
//import './../js/functions.js'

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      width: window.innerWidth,
      height: window.innerHeight
    }
  }

  updateDimensions() {
    if (this.state.width !== window.innerWidth) {
      this.setState({ width: window.innerWidth, height: window.innerHeight });
    } 
  }
  
  render(){
    return (
        <Router basename={process.env.PUBLIC_URL}>
          <Switch>
            <Route exact path="/">
              <Home canvasWidth={this.state.width} canvasHeight={this.state.height} />
            </Route>
            <Route component={() => { return <NotFound /> }}></Route>
          </Switch>
        </Router>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    currentUser: state.currentUser,
    currentBackgroundAngle: state.currentBackgroundAngle
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: (currentUser) => { dispatch(setUser(currentUser)) },
    changeBackgroundAngle: (angle) => { dispatch(changeBackgroundAngle(angle)) },
    setConfigData: (data) => { dispatch(setConfigData(data)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)