import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import Modal from './Modal'
import { configButton1Active, configButton1Inactive, configButton2Active, configButton2Inactive } from './../../js/images'

import { changeBackground, togglePanel, changeObjectSize } from './../../redux/actionCreators'


let backgroundFormat = [
    {
        event: "/v1602549201/Handball%20manager/canvas-1_rbigwq.jpg"
    },
    {
        event: "/v1605608953/Handball%20manager/cancha4_anaat0.png"
    },
    {
        event: "/v1605608956/Handball%20manager/cancha5_vofblr.png"
    },
    {
        event: "/v1605608955/Handball%20manager/cancha6_lwyjfv.png"
    },
]

let backgroundColor = [
    {
        event: "/v1602549201/Handball%20manager/canvas-1_rbigwq.jpg",
    },
    {
        event: "/v1602548589/Handball%20manager/canvas-2_ao97dx.jpg",
    },
    {
        event: "/v1602548592/Handball%20manager/canvas-3_uowaon.jpg",
    },
]

const ConfigPanel = ({panels, togglePanel, changeBackground, currentUser, changeObjectSize}) => {
   
    const [panel, setPanel] = useState("General")

    return (  
        <div className="config-panel" style={panels[6].showPanel ? { transform: "scaleX(1)", zIndex: 10 } : { transform: "scaleX(0)" }}>
            <div className="draw-panel__exit-container" onClick={() => {togglePanel(6)}}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <div className="config-panel__buttons-container">
                <button 
                    className="config-panel__button-1" 
                    style={
                        panel === "General" ? {backgroundImage: `url(${configButton1Active})`}
                                            : { backgroundImage: `url(${configButton1Inactive})`}
                    } 
                    onClick={() => {setPanel("General")}}
                >
                    General
                </button>
                <button 
                    className="config-panel__button-2" 
                    style={
                        panel === "Equipos" ? { backgroundImage: `url(${configButton2Active})`}
                                            : { backgroundImage: `url(${configButton2Inactive})`}
                    } 
                    onClick={() => {setPanel("Equipos")}}
                >
                    Equipos
                </button>
            </div>
            <div className="config-panel__general" style={panel === "General" ? {display: "block"} : {display: "none"}}>
                <h2 className="config-panel__subtitle">Formato de pista</h2>
                <ul className="config-panel__list">
                    {
                        backgroundFormat.map((image, key) => {
                            return (
                                <li className="config-panel__item" onClick={() => { changeBackground(image.event) }} key={key}>
                                    <div className={`config-panel__item-image-${key + 1}`}></div>
                                </li>
                            )
                        })
                    }
                </ul>
                <h2 className="config-panel__subtitle">Color de pista</h2>
                <ul className="config-panel__list">
                    {
                        backgroundColor.map((image, key) => {
                            return (
                                <li className="config-panel__item" onClick={() => { changeBackground(image.event) }} key={key}>
                                    <div className={`config-panel__item-image-color-${key + 1}`}></div>
                                </li>
                            )
                        })
                    }
                </ul>
                <h2 className="config-panel__subtitle">Jugadores</h2>
                <ul>

                </ul>
                <h2 className="config-panel__subtitle">Tamaño</h2>
                <ul className="config-panel__size-list">
                    <li className="config-panel__size-item" onClick={() => { changeObjectSize("normal") } }>
                        <div className="config-panel__size-item">Normal</div>
                    </li>
                    <li className="config-panel__size-item" onClick={() => { changeObjectSize("medium") } }>
                        <div className="config-panel__size-item">Mediano</div>
                    </li>
                    <li className="config-panel__size-item" onClick={() => { changeObjectSize("small") } }>
                        <div className="config-panel__size-item">Pequeño</div>
                    </li>
                </ul>
            </div>

            {/* ---------------------------------- Panel de configuracion de equipos --------------------------- */}

            <div className="config-panel__teams" style={panel === "Equipos" ? { display: "block" } : { display: "none" }}>
                <h2 className="config-panel__subtitle">Gestionar equipos</h2>
                <Modal />
                <ul className="">
                    {
                        currentUser ?
                        currentUser.customTeams.map((team, key) => {
                            return (
                                <li key={key} className="config-panel__teams-item">
                                    {team.teamName}
                                </li>
                            )
                        })
                        : null
                    }
                </ul>
                <h2 className="config-panel__subtitle">Gestionar posiciones</h2>
                <ul className="">
                    {
                        currentUser.customTeams.length > 0 ?
                        currentUser.customTeams.forEach(team => {
                            team.players.map((player, key) => {
                                return (
                                    <li key={key} className="config-panel__players-item">
                                        <label htmlFor={`player-${key+1}`} className="config-panel__players-label">{player.name}</label>
                                        <select name="positions" id="positions">
                                            <option value="lateral-left">Lateral izquierdo</option>
                                            <option value="lateral-right">Lateral derecho</option>
                                            <option value="central">Central</option>
                                            <option value="extreme-left">Extremo izquierdo</option>
                                            <option value="extreme-right">Extremo derecho</option>
                                            <option value="pivot">Pivote</option>
                                            <option value="archer">Arquero</option>
                                        </select>
                                    </li>
                                )
                            })
                        }) :
                        <span>No tiene equipos creados</span>
                    }
                </ul>

                <h2 className="config-panel__subtitle">Equipo atacante</h2>
                <span className="">Número de jugadores</span>
                <span className="">Forma y color</span>

                <h2 className="config-panel__subtitle">Equipo defensor</h2>
                <span className="">Número de jugadores</span>
                <span className="">Forma y color</span>

            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        panels: state.panels,
        currentUser: state.currentUser
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeBackground: (background) => { dispatch(changeBackground(background)) },
        togglePanel: (panel) => {dispatch(togglePanel(panel))},
        changeObjectSize: (size) => { dispatch(changeObjectSize(size)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(ConfigPanel);