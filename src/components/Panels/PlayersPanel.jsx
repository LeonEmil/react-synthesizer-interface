import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { toggleDefaultPlayersBlue, togglePanel } from '../../redux/actionCreators';
import { toggleDefaultPlayersRed } from '../../redux/actionCreators';
import { connect } from 'react-redux'

let playersBlue = [
    {
        event: 0
    },
    {
        event: 1
    },
    {
        event: 2
    },
    {
        event: 3
    },
    {
        event: 4
    },
    {
        event: 5
    },
    {
        event: 6
    },
    {
        event: 7
    },
]

let playersRed = [
    {
        event: 0
    },
    {
        event: 1
    },
    {
        event: 2
    },
    {
        event: 3
    },
    {
        event: 4
    },
    {
        event: 5
    },
    {
        event: 6
    },
    {
        event: 7
    },
]

const PlayersPanel = ({panels, togglePanel, toggleDefaultPlayersBlue, toggleDefaultPlayersRed, menuRef, configData, defaultPlayersBlue, defaultPlayersRed }) => {
    const panelSize = window.innerWidth < 576 ? '-300px' : '-500px'
    return (  
        <div className="players-panel" style={panels[2].showPanel ? { left: `${menuRef.current.offsetWidth}px`, zIndex: 10 } : { left: panelSize }}>
            <div className="draw-panel__exit-container" onClick={() => {togglePanel(2)}}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <h2 className="players-panel__subtitle">Atacante</h2>
            <ul className="players-panel__list">
                {
                    playersBlue.map((player, key) => {
                        return (
                            <li className="players-panel__item" onClick={() => { toggleDefaultPlayersBlue(player.event) }} key={key}>
                                <span className="object-panel__item-counter">{defaultPlayersBlue[player.event].length}</span>
                                <div className={`players-panel__item-blue-${key + 1}`}></div>
                            </li>
                        )
                    })
                }
            </ul>
            <h2 className="players-panel__subtitle">Defensor</h2>
            <ul className="players-panel__list">
                {
                    playersRed.map((player, key) => {
                        return (
                            <li className="players-panel__item" onClick={() => { toggleDefaultPlayersRed(player.event) }} key={key}>
                                <span className="object-panel__item-counter">{defaultPlayersRed[player.event].length}</span>
                                <div className={`players-panel__item-red-${key + 1}`}></div>
                            </li>
                        )
                    })
                }
            </ul>
            {
                configData.bannerUrl !== "" ?
                    <img src={configData.bannerUrl} alt={configData.BannerAlt} className="banner-panel" />
                    : null
            }
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        panels: state.panels,
        configData: state.configData,
        defaultPlayersBlue: state.history[state.historyIndex].defaultPlayersBlue,
        defaultPlayersRed: state.history[state.historyIndex].defaultPlayersRed
    }
}

const mapDispatchToProps = dispatch => {
    return {
        toggleDefaultPlayersBlue: (playersType) => { dispatch(toggleDefaultPlayersBlue(playersType)) },
        toggleDefaultPlayersRed: (playersType) => { dispatch(toggleDefaultPlayersRed(playersType)) },
        togglePanel: (panel) => { dispatch(togglePanel(panel)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(PlayersPanel);