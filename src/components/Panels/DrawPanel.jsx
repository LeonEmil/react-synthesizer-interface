import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

import { connect } from 'react-redux'
import { changeLineColor, changeLineType, togglePanel } from './../../redux/actionCreators'

let lines = [
    {
        name: "linea 1",
        className: "",
        dash: false,
    },
    {
        name: "linea 2",
        className: "",
        dash: true,
    },
    {
        name: "linea 3",
        className: "",
        dash: false,
    },
    {
        name: "linea 4",
        className: "",
        dash: true,
    },
    {
        name: "linea 5",
        className: "",
        dash: false,
    },
    {
        name: "linea 6",
        className: "",
        dash: true,
    },
    {
        name: "linea 7",
        className: "",
        dash: false,
    },
    {
        name: "linea 8",
        className: "",
        dash: true,
    },
]

let colorSelectorList = [
    {
        id: 1,
        event: '#384e77'
    },
    {
        id: 2,
        event: '#cc2916'
    },
    {
        id: 3,
        event: '#682477'
    },
    {
        id: 4,
        event: '#875a1f'
    },
    {
        id: 5,
        event: '#696566'
    },
    {
        id: 6,
        event: '#000000'
    },
    {
        id: 7,
        event: '#299035'
    },
    {
        id: 8,
        event: '#ebe22d'
    },
    {
        id: 9,
        event: '#ef841a'
    },
    {
        id: 10,
        event: '#bbbab8'
    },
    {
        id: 11,
        event: '#5e1914'
    },
    {
        id: 12,
        event: '#54bee6'
    },
]

const DrawPanel = ({panels, changeLineColor, changeLineType, togglePanel, menuRef, configData }) => {
    const panelSize = window.innerWidth < 576 ? '-300px' : '-500px'

    const [ lineSelected, setLineSelected ] = useState()
    const [ colorSelected, setColorSelected ] = useState()

    return (  
        <div className="draw-panel" id="draw-panel" style={panels[1].showPanel ? { left: `${menuRef.current.offsetWidth}px`, zIndex: 10} : {left: panelSize}}>
            <div className="draw-panel__exit-container" onClick={() => { togglePanel(1) }}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item"/>
            </div>
            <ul className="draw-panel__list">
                {
                    lines.map((line, key) => {
                        return (
                            <li className="draw-panel__item" 
                                style={lineSelected === line.name ? {backgroundColor: "#d9e29f"} : null}
                                onClick={
                                    () => {
                                        changeLineType(line.name)
                                        setLineSelected(line.name)
                                    } 
                                } 
                                key={key}
                            >
                                <div className={`draw-panel__item-image-${key + 1}`}></div>
                                <span className={`draw-panel__item-span-${key+1}`}>{line.name}</span>
                            </li>
                        )
                    })
                }
            </ul>
            <div className="draw-panel__color-selector-container">
                <h2 className="draw-panel__color-selector-title">Color del trazo</h2>
                <ul className="draw-panel__color-selector-list">
                    {
                        colorSelectorList.map((color, key) => {
                            return (
                                <li className="draw-panel__color-selector-item" 
                                    style={colorSelected === color.id? { boxShadow: "0 0 0 5px white, 0 0 3px 7px gray" } : null}
                                    onClick={() => { 
                                        changeLineColor(color.event)
                                        setColorSelected(color.id) 
                                    }} 
                                    key={key}
                                >
                                    <div className={`draw-panel__color-selector-option-${color.id}`}></div>
                                </li>
                            )
                        })
                    }
                </ul>
                {
                    configData.bannerUrl !== "" ?
                        <img src={configData.bannerUrl} alt={configData.BannerAlt} className="banner-panel" />
                        : null
                }
            </div>
            {/* <div className="draw-panel__banner"></div> */}
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        panels: state.panels,
        configData: state.configData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeLineColor: (color) => { dispatch(changeLineColor(color)) },
        changeLineType: (lineType) => { dispatch(changeLineType(lineType)) },
        togglePanel: (panel) => { dispatch(togglePanel(panel)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(DrawPanel);