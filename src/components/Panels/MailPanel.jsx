import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { togglePanel } from '../../redux/actionCreators';
import { useFormik } from 'formik'
import * as Yup from 'yup'
import emailjs from 'emailjs'

const MailPanel = ({panels, togglePanel, currentUser}) => {

    const { handleSubmit, handleChange, handleBlur, values, errors, touched } = useFormik({
        initialValues: {
            name: '',
            email: '',
            subject: '',
            message: ''
        },
        validationSchema: Yup.object({
            name: Yup.string().required('Por favor ingrese su nombre'),
            email: Yup.string().email("Por favor ingrese un email válido").required('Por favor ingrese el email del destinatario'),
            message: Yup.string().required('Por favor ingrese un mensaje'),
        }),
        onSubmit: ({ name, email, subject, message }) => {

            emailjs.send("service_5ofv86m", "template_obif4gn", {
                subject: subject,
                name: name,
                email: email,
                message: message,
            })
                .then((result) => {
                    alert("Mensaje enviado")
                }, (error) => {
                    alert(error.message)
                });  
                
        }
    })

    return (  
        <div className="mail-panel" style={panels[5].showPanel ? { transform: "scaleX(1)", zIndex: 10 } : { transform: "scaleX(0)" }}>
            <div className="draw-panel__exit-container" onClick={() => {togglePanel(5)}}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <form className="mail__form">
                <label htmlFor="name" className="mail__label">Nombre</label>
                <input 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="text" 
                    className="mail__input" 
                    id="name"
                    value={values.name}
                    required
                />
                {
                    errors.name && touched.name ?
                        <span className="login__alert">{errors.name}</span>
                        : null
                }

                <label htmlFor="email" className="mail__label">E-mail</label>
                <input 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="email" 
                    className="mail__input" 
                    id="email"
                    value={values.email}
                    required
                />
                {
                    errors.email && touched.email ?
                        <span className="login__alert">{errors.email}</span>
                        : null
                }

                <label htmlFor="subject" className="mail__label">Asunto</label>
                <input 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="text" 
                    className="mail__input" 
                    id="subject"
                    value={values.subject}
                    required
                />
                {
                    errors.subject && touched.subject ?
                        <span className="login__alert">{errors.subject}</span>
                        : null
                }

                <label htmlFor="message" className="mail__label">Mensaje</label>
                <textarea 
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="text" 
                    className="mail__input mail__input-textarea" 
                    id="message"
                    value={values.message}
                    required
                />
                {
                    errors.message && touched.message ?
                        <span className="login__alert">{errors.message}</span>
                        : null
                }

                <input type="submit" value="Enviar" className="mail__button"/>
            </form>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        panels: state.panels
    }
}

const mapDispatchToProps = dispatch => {
    return {
        togglePanel: (panel) => {dispatch(togglePanel(panel))}
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(MailPanel);