import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import StrategyList from './StrategyList'
import { togglePanel } from './../../redux/actionCreators'

let strategies = [
    {
        name: "Estrategia 1",
        date: "23 de julio de 2020",
        size: "365 Kb",
        link: "https://res.cloudinary.com/leonemil/image/upload/v1603566280/Handball%20manager/strategy_d934du.png"
    },
    {
        name: "Estrategia 2",
        date: "23 de julio de 2020",
        size: "365 Kb",
        link: "https://res.cloudinary.com/leonemil/image/upload/v1603566280/Handball%20manager/strategy_d934du.png"
    },
    {
        name: "Estrategia 3",
        date: "23 de julio de 2020",
        size: "365 Kb",
        link: "https://res.cloudinary.com/leonemil/image/upload/v1603566280/Handball%20manager/strategy_d934du.png"
    },
    {
        name: "Estrategia 4",
        date: "23 de julio de 2020",
        size: "365 Kb",
        link: "https://res.cloudinary.com/leonemil/image/upload/v1603566280/Handball%20manager/strategy_d934du.png"
    },
]

const StrategyPanel = ({panels, togglePanel, currentUser, menuRef, configData}) => {
    const panelSize = window.innerWidth < 576 ? '-300px' : '-500px'
    return (  
        <div className="strategy-panel" style={panels[0].showPanel ? { left: `${menuRef.current.offsetWidth}px`, zIndex: 10 } : { left: panelSize }}>
            <div className="draw-panel__exit-container" onClick={() => { togglePanel(0) }}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <ul className="draw-panel__list">
                {
                    currentUser.strategies.map((item, key) => {
                        return (
                            <li className="strategy-panel__item" onClick={() => { }} key={key}>
                                <a href={item.url} target="_blank" rel="noopener noreferrer" className="strategy-panel__item-link">
                                    <span className={`draw-panel__item-span-${key + 1}`}>{item.name}</span>
                                    <span className={`draw-panel__item-span-${key + 1} text-gray`}>{item.date}</span>
                                    <span className={`draw-panel__item-span-${key + 1} text-gray`}>{item.size}</span>
                                </a>
                            </li>
                        )
                    })
                }
            </ul>
            {
                configData.bannerUrl !== "" ?
                    <img src={configData.bannerUrl} alt={configData.BannerAlt} className="banner-panel"/>
                : null
            }
            {/* <button className="draw-panel__button">Crear nueva carpeta</button> */}

        </div>
    );
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        panels: state.panels,
        configData: state.configData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        togglePanel: (panel) => { dispatch(togglePanel(panel)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(StrategyPanel);