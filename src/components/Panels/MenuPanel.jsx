import React from 'react'
import { connect } from 'react-redux'
import { togglePanel } from './../../redux/actionCreators'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import menuPanelList from './../../js/menuPanelList'

const MenuPanel = ({panels, togglePanel, menuRef}) => {
    return (
        <div className="menu-panel" style={panels[9].showPanel ? { left: menuRef.current ? `${menuRef.current.offsetWidth}px` : "60px", zIndex: 10 } : { left: '-500px' }}>
            <div className="draw-panel__exit-container" onClick={() => { togglePanel(9) }}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <ul className="menu-panel__list">
                {
                    menuPanelList.map((item, key) => {
                        return (
                            <li key={key} className="menu-panel__item" onClick={item.action}>{item.name}</li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        panels: state.panels
    }
}

const mapDistpatchToProps = (dispatch) => {
    return {
        togglePanel: (panel) => { dispatch(togglePanel(panel)) }
    }
}

export default connect(mapStateToProps, mapDistpatchToProps)(MenuPanel)