import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { togglePanel } from '../../redux/actionCreators';

const VideoPanel = ({ panels, togglePanel, menuRef }) => {

    const start = useRef()
    const stop = useRef()
    const video = useRef()

    useEffect(() => {
        let recorder, stream;
        
        async function startRecording() {
            stream = await navigator.mediaDevices.getDisplayMedia({
                video: { mediaSource: "screen" },
                audio: {
                    echoCancellation: true,
                    noiseSuppression: true,
                    sampleRate: 44100
                }
            });
            recorder = new MediaRecorder(stream);
        
            const chunks = [];
            recorder.ondataavailable = e => chunks.push(e.data);
            recorder.onstop = e => {
                const completeBlob = new Blob(chunks, { type: chunks[0].type });
                video.current.src = URL.createObjectURL(completeBlob);
            };
        
            recorder.start();
        }
            
        start.current.addEventListener("click", () => {
            start.current.setAttribute("disabled", true);
            stop.current.removeAttribute("disabled");
        
            startRecording();
        });
        
        stop.current.addEventListener("click", () => {
            stop.current.setAttribute("disabled", true);
            start.current.removeAttribute("disabled");
        
            recorder.stop();
            stream.getVideoTracks()[0].stop();
        });
    },[])

    const handleStart = () => {

    }

    return (  
        <>
            <div className="video-panel" style={panels[8].showPanel ? { left: `${menuRef.current.offsetWidth}px`, zIndex: 10 } : { left: '-500px' }}>
                <div className="draw-panel__exit-container" onClick={() => { togglePanel(8) }}>
                    <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
                </div>
            <button className="waves-effect waves-light btn" ref={start}>
                Grabar
            </button>
            <button className="waves-effect waves-light btn" ref={stop} disabled>
                Detener
            </button>
            <video className="video" controls autoPlay ref={video}/>
        </div>
    </>
    );
}

const mapStateToProps = (state) => {
    return {
        panels: state.panels
    }
}

const mapDispatchToProps = dispatch => {
    return {
        togglePanel: (panel) => { dispatch(togglePanel(panel)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(VideoPanel);