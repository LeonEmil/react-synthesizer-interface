import React, { useEffect, useRef } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import M from "materialize-css";
import "materialize-css/dist/css/materialize.min.css";
import { connect } from 'react-redux'
import { auth, db } from '../../firebase/firebase'
import { useFormik } from 'formik'
import * as Yup from 'yup'

const Modal = ({currentUser}) => {

    const modalRef = useRef()

    useEffect(() => {
        const options = {
            onOpenStart: () => {
                console.log("Open Start");
            },
            onOpenEnd: () => {
                console.log("Open End");
            },
            onCloseStart: () => {
                console.log("Close Start");
            },
            onCloseEnd: () => {
                console.log("Close End");
            },
            inDuration: 250,
            outDuration: 250,
            opacity: 0.5,
            dismissible: false,
            startingTop: "4%",
            endingTop: "10%"
        };
        const modal1 = document.getElementById("modal1")
        M.Modal.init(modalRef.current, options);
        console.log(modal1)
    }, [])

    const { handleSubmit, handleChange, handleBlur, values, touched, errors } = useFormik({
        initialValues: {
            teamName: '',
            name1: '',
            name2: '',
            name3: '',
            name4: '',
            name5: '',
            name6: '',
            name7: '',
        },
        validationSchema: Yup.object({
            teamName: Yup.string().max(15, 'El nombre del equipo debe ser más corto de 15 caracteres').required('Ingrese el nombre del equipo'),
        }),
        onSubmit: ({teamName, name1, name2, name3, name4, name5, name6, name7}) => {
            console.log("submit")
            db.collection('users').where("email", '==', currentUser.email).get().then(response => {
                const id = response.docs[0].id
                let user = response.docs[0].data()
                user.customTeams.push({
                    teamName: teamName,
                    players: [
                        {
                            name: name1,
                            number: 1
                        },
                        {
                            name: name2,
                            number: 2
                        },
                        {
                            name: name3,
                            number: 3
                        },
                        {
                            name: name4,
                            number: 4
                        },
                        {
                            name: name5,
                            number: 5
                        },
                        {
                            name: name6,
                            number: 6
                        },
                        {
                            name: name7,
                            number: 7
                        },
                    ]
                })
                db.collection('users').doc(id).update(user).then(() => {
                    alert("Equipo creado")
                })
            })
        }
    })

    return (
            <>
                <button
                    className="waves-effect waves-light btn modal-trigger"
                    data-target="modal1"
                >
                    Crear equipo
                </button>

                <div
                    ref={modalRef}
                    id="modal1"
                    className="modal"
                >
                    <div className="modal-content">
                        <div className="draw-panel__exit-container modal-close waves-effect waves-green btn-flat">
                            <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
                        </div>
                        <h4>Crear equipo</h4>
                        <form onSubmit={handleSubmit}>
                            <div className="form-teams__create-player">
                                <label htmlFor="teamName" className="form-teams__label">Nombre del equipo</label>
                                <input 
                                    value={values.teamName}
                                    onChange={handleChange}
                                    onBlur={handleBlur} 
                                    type="text" 
                                    name="teamName" 
                                    id="teamName" 
                                    required
                                />
                            </div>
                            {
                                [1,2,3,4,5,6,7].map((player, key) => {
                                    return (
                                        <div className="form-teams__create-player" key={key}>
                                            <label htmlFor={`name${player}`} className="form-teams__label">Nombre del jugador {player}</label>
                                            <input
                                                value={values[`name${player}`]}
                                                onChange={handleChange}
                                                onBlur={handleBlur} 
                                                type="text" 
                                                name={`name${player}`} 
                                                id={`name${player}`} 
                                                required
                                            />
                                        </div>
                                    )
                                })
                            }
                            <input type="submit" value="Crear"/>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button className="modal-close waves-effect waves-green btn-flat">
                            Cancelar
                        </button>
                    </div>
                </div>
            </>
        );
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser
    }
}

export default connect(mapStateToProps, null)(Modal);